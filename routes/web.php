<?php

use Illuminate\Support\Facades\Route;

// home
Route::view('/', 'home');

// about
Route::view('/about', 'about');

// contact
Route::view('/contact', 'contact');

// projects and activities
//Route::view('/projects-and-activities', 'projects_and_activities');

// yearly projects and activities
Route::view('/activities', 'projects_and_activities');

// regions
Route::view('/regions/yangon', 'regions.yangon');
Route::view('/regions/mandalay', 'regions.mandalay');
Route::view('/regions/taungyi', 'regions.taungyi');
Route::view('/regions/mgway', 'regions.mgway');

// activities
Route::get('/projects-and-activities', 'ActivityController@getAllActivities');
// individual activity
Route::get('/projects-and-activities/{year}/{id}', 'ActivityController@show');
// yearly activities
Route::get('/projects-and-activities/{year}', 'ActivityController@index');

// admin
// Route::prefix('/apple-orange/admin')->group(function () {

//     // Route::get('/admin/posts/create', function () {
//     //     return view('admin.posts.post');
//     // });

//     Route::view('/posts/create', 'admin.posts.create-post');

//     Route::namespace('Admin')->group(function () {
//         Route::post('/posts', 'PostController@store');
//     });
// });

Route::prefix('/apple-orange/admin/')->group(function () {
    // Route::view('posts', 'admin.posts.posts');
    Route::view('posts/create', 'admin.posts.create-post');
});

Route::prefix('/apple-orange/admin/')->namespace('Admin')->group(function () {
    // Route::post('/posts', 'PostController@store');
    Route::delete('posts/{post}', 'PostController@destroy')->name('post-delete');
    Route::get('posts', 'PostController@index');
    Route::post('posts', 'PostController@store');
});
