<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Activity;
use Faker\Generator as Faker;
use Intervention\Image\Facades\Image;

$factory->define(Activity::class, function (Faker $faker) {
    $date_and_time = $faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now');
    $date_and_time = $date_and_time->format('Y-m-d H:i:s');
    $date = explode(' ', $date_and_time)[0];
    $year = explode('-', $date)[0];

    $img_arr = fakeImages($faker);

    return [
        'title' => $faker->text(30),
        'body' => $faker->text(20),
        'images' => json_encode($img_arr),
        'thumbnails' => json_encode($img_arr),
        'date' => $date,
        'year' => $year,
        'user_id' => \App\User::all()->random()->id
    ];
});

function fakeImages($faker, $length = 3)
{
    $imgs_arr = array();

    for ($i = 0; $i < $length; $i++) {

        $original_image = $faker->image('public/storage/images/', 640, 480, 'cats');
        $thumbnail_image = Image::make($original_image)->fit(140, 100);

        $thumbnail_name = $faker->regexify('[A-Za-z0-9]{15}') . '.jpg';
        $thumbnail_image->save('public/storage/thumbnails/' . $thumbnail_name);

        rename($original_image, 'public/storage/images/' . $thumbnail_name);

        array_push($imgs_arr, $thumbnail_name);
    }

    return $imgs_arr;
}
