<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Activity;

const START_YEAR = 2016;
const COUNT_OF_ACTIVITY_IN_CURRENT_YEAR = 6;
const COUNT_OF_ACTIVITY_IN_PAST_YEAR = 3;
const LATEST_ACTIVITIES_COUNT = 5;

class ActivityController extends Controller
{

    public function __construct(Request $request)
    {
        $this->_current_year = (int) date('Y');

        $year = $request->route()->parameter('year');
        if (!empty($year)) {
            $this->_year = $this->_isYearValid($year);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $activities = Activity::where('year', $this->_year)->orderBy('date', 'desc')->paginate(6);

        $activities = $this->_mutateImages($activities);

        $activities->year = $this->_year;

        return view('activities.year.activities')->with('activities', $activities);
    }

    private function _isYearValid($year)
    {
        if (!ctype_digit($year)) return false;

        $year = (int) $year;

        for ($i = START_YEAR; $i <= $this->_current_year; $i++) {
            if ($year === $i) {
                return $year;
            }
        }

        return false;
    }

    public function getAllActivities()
    {
        $activities = array();

        for ($year = START_YEAR; $year <= $this->_current_year; $year++) {
            $activities[(string) $year] = $this->_getActivities($year);
        }

        return view('activities.activities')->with('activities', $activities);
    }

    private function _getActivities($year)
    {
        $count = $year === $this->_current_year ? COUNT_OF_ACTIVITY_IN_CURRENT_YEAR : COUNT_OF_ACTIVITY_IN_PAST_YEAR;

        $activities = Activity::where('year', $year)->orderBy('date', 'desc')->take($count)->get();

        $activities->year = $year;

        return $this->_mutateImages($activities);
    }

    private function _mutateImages($activities)
    {

        foreach ($activities as $activity) {
            $activity['thumbnails'] = json_decode($activity['thumbnails']);
            $activity['images'] = json_decode($activity['images']);
        }

        return $activities;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($year, $id)
    {
        $activity = Activity::where('id', $id)->get();
        $activity = $this->_mutateImages($activity)[0];

        $latest_activities = Activity::where('id', '!=', $id)->orderBy('date', 'desc')->select('date', 'title', 'id', 'year')->take(LATEST_ACTIVITIES_COUNT)->get();

        $activity['latest-activities'] = $latest_activities;

        return view('activities.year.activity')->with('activity', $activity);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
