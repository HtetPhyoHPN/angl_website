<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Activity;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class PostController extends Controller
{

    public function __construct()
    {
        $this->_current_year = (int) date('Y');
        $this->_previous_year = $this->_current_year - 1;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this_year_posts = Activity::select('id', 'title', 'date', 'body')->where('year', "$this->_current_year")->orderBy('date', 'desc')->get();
        $previous_year_posts = Activity::select('id', 'title', 'date', 'body')->where('year', "$this->_previous_year")->orderBy('date', 'desc')->get();

        $posts = [
            'this_year' => [
                'posts' => $this_year_posts,
                'count' => count($this_year_posts),
                'year' => $this->_current_year
            ],
            'prev_year' => [
                'posts' => $previous_year_posts,
                'count' => count($previous_year_posts),
                'year' => $this->_previous_year
            ]
        ];

        return view('admin.posts.posts')->with('posts', $posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->isMethod('POST')) {

            $rules = [
                'title' => 'required|min:6',
                'body' => 'required|min:10',
                'date' => 'required|date',
                'image' => 'required',
                'image.*' => 'required|mimes:jpeg,png,jpg,gif',
            ];

            $messages = [
                'required' => "This :attribute field is required.",
                'image.mimes' => 'Unsupported image type! The :attribute must be one of the following types: :values.',
            ];

            $this->validate($request, $rules, $messages);

            if (!empty($request->image)) {
                $request->image = $this->storeAndGetSerializedImageJsonObject($request->file('image'));
            }

            $activity = new Activity();
            $has_stored = $activity->store($request);

            if ($has_stored) {
                return back()->with('success', 'Post uploaded successfully!');
            }
            return back()->with('error', 'Something went wrong! Please try again later.');
        }
    }

    private function storeAndGetSerializedImageJsonObject($image_arr = array())
    {
        $output_img_arr = array();
        foreach ($image_arr as $file) {

            $file_name_with_ext = $file->getClientOriginalName();
            $file_name_to_store = $this->serializeImageName($file_name_with_ext);
            Image::make($file->getRealPath())->save('storage/images/' . $file_name_to_store, 100);
            Image::make($file->getRealPath())->fit(140, 100)->save('storage/thumbnails/' . $file_name_to_store);

            array_push($output_img_arr, $file_name_to_store);
        }
        return json_encode($output_img_arr);
    }

    private function serializeImageName($name)
    {
        return date('mdYHis') . uniqid() . str_replace(' ', '', $name);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $has_deleted = Activity::where('id', $id)->delete();

        if ($has_deleted) {
            return back()->with('success', 'Post deleted successfully!');
        }
        return back()->with('error', 'Something went wrong! Please try again later.');
    }
}
