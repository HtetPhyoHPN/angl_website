<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function store($request)
    {
        $this->title = $request->title;
        $this->body = $request->body;
        $this->date = $request->date;
        $this->year = explode('-', $request->date)[0];
        $this->user_id = 1;

        if (!empty($request->image)) {
            $this->images = $request->image;
            $this->thumbnails = $request->image;
        }

        if ($this->save()) {
            return true;
        }
        return false;
    }
}
