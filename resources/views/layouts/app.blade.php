<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />

    <!-- favicon -->
    <link rel="icon" href={{ asset("favicon.ico") }} />

    <title>

@if(Request::is('/'))         
    ANGL | Home
@elseif(Request::is('regions/yangon'))
    ANGL | Region-Yangon
@elseif(Request::is('regions/mandalay'))
    ANGL | Region-Mandalay
@elseif(Request::is('regions/taungyi'))
    ANGL | Region-Taungyi
@elseif(Request::is('regions/mgway'))
    ANGL | Region-Mgway
@elseif(Request::is('projects-and-activities/*') || Request::is('projects-and-activities'))
    ANGL | Projects & Activities
@elseif(Request::is('about'))
    ANGL | About
@elseif(Request::is('contact'))
    ANGL | Contact
@endif

    </title>

    <!-- slick css -->
    <link rel="stylesheet" type="text/css" href={{ asset("assets/slick/slick.css") }} />

    <!-- fancybox css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css" />

    <!-- nav and landing first home page css -->
    <link rel="stylesheet" type="text/css" href={{ asset("assets/css/nav-and-landing.css") }} />

    <!-- body css -->
    <link rel="stylesheet" type="text/css" href={{ asset("assets/css/style.css") }} />

    <style>
        .affix {
          position: sticky;
          top: 100px;
          right: 0;
        }
      </style>
</head>

<body>

    <!-- # nav bar -->
    <div class="topnav-wrapper">
        <div class="topnav">
            <a href="/" class="{{ Request::is('/') ? 'active' : '' }}">Home</a>
            <div class="dropdown {{ Request::is('regions/yangon') || Request::is('regions/mandalay') || Request::is('regions/taungyi') || Request::is('regions/mgway') ? 'active' : '' }}">
                <button class="dropbtn">
                        <span class="{{ Request::is('regions/yangon') || Request::is('regions/mandalay') || Request::is('regions/taungyi') || Request::is('regions/mgway') ? 'text-white' : '' }}">Regions</span>
                        <?xml version="1.0" encoding="iso-8859-1"?>
                        <svg version="1.1" id="down-arrow-navbar" xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 330 330"
                            style="enable-background:new 0 0 330 330;" xml:space="preserve">
                            <path id="downarrow-navbar-path" d="M325.607,79.393c-5.857-5.857-15.355-5.858-21.213,0.001l-139.39,139.393L25.607,79.393
    c-5.857-5.857-15.355-5.858-21.213,0.001c-5.858,5.858-5.858,15.355,0,21.213l150.004,150c2.813,2.813,6.628,4.393,10.606,4.393
    s7.794-1.581,10.606-4.394l149.996-150C331.465,94.749,331.465,85.251,325.607,79.393z" />
                        </svg>
                    </button>
                <div class="my-dropdown-content">
                    <a href="/regions/yangon">Yangon</a>
                    <a href="/regions/mandalay">Mandalay</a>
                    <a href="/regions/taungyi">Taungyi</a>
                    <a href="/regions/mgway">Mgway</a>
                </div>
            </div>
            <div class="dropdown {{ Request::is('projects-and-activities/*') || Request::is('projects-and-activities') ? 'active' : '' }}">
                <button class="dropbtn">
                        <span onclick="goToPjsAndAct()" class="{{ Request::is('projects-and-activities/*') || Request::is('projects-and-activities') ? 'text-white' : '' }}">Activities</span>
                        <?xml version="1.0" encoding="iso-8859-1"?>
                        <svg version="1.1" id="down-arrow-navbar" xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 330 330"
                            style="enable-background:new 0 0 330 330;" xml:space="preserve">
                            <path id="downarrow-navbar-path" d="M325.607,79.393c-5.857-5.857-15.355-5.858-21.213,0.001l-139.39,139.393L25.607,79.393
            c-5.857-5.857-15.355-5.858-21.213,0.001c-5.858,5.858-5.858,15.355,0,21.213l150.004,150c2.813,2.813,6.628,4.393,10.606,4.393
            s7.794-1.581,10.606-4.394l149.996-150C331.465,94.749,331.465,85.251,325.607,79.393z" />
                        </svg>
                    </button>
                <div class="my-dropdown-content">
                    <a href="/projects-and-activities/2019">2019 activities</a>
                    <a href="/projects-and-activities/2018">2018 activities</a>
                    <a href="/projects-and-activities/2017">2017 activities</a>
                    <a href="/projects-and-activities/2016">2016 activities</a>
                </div>
            </div>
            <div class="dropdown {{ Request::is('about') ? 'active' : '' }}">
                <button class="dropbtn">
                        <!-- <a href="about.html" class="pt-0 pl-0 pb-0 pr-2">About</a> -->
                        <span onclick="goToAbout()" class="{{ Request::is('about') ? 'text-white' : '' }}">About</span>
                        <?xml version="1.0" encoding="iso-8859-1"?>
                        <svg version="1.1" id="down-arrow-navbar" xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 330 330"
                            style="enable-background:new 0 0 330 330;" xml:space="preserve">
                            <path id="downarrow-navbar-path" d="M325.607,79.393c-5.857-5.857-15.355-5.858-21.213,0.001l-139.39,139.393L25.607,79.393
    c-5.857-5.857-15.355-5.858-21.213,0.001c-5.858,5.858-5.858,15.355,0,21.213l150.004,150c2.813,2.813,6.628,4.393,10.606,4.393
    s7.794-1.581,10.606-4.394l149.996-150C331.465,94.749,331.465,85.251,325.607,79.393z" />
                        </svg>
                    </button>
                <div class="my-dropdown-content">
                    <a href="/about#what-is-angl">What's ANGL?</a>
                    <a href="/about#history">History</a>
                    <a href="/about#mission">Mission/Vision</a>
                    <a href="/about#constitution">Constitution</a>
                </div>
            </div>
            <a href="/contact" class="{{ Request::is('contact') ? 'active' : '' }}">Contact</a>
            <a href="javascript:void(0);" style="font-size:15px;" class="icon" id="bar-icon" onclick="myFunction()">&#9776;</a>
        </div>
    </div>
    <!-- end nav bar -->

    @yield('content')


    <!-- # footer -->
    <footer class="footer mt-4 pb-5">
        <div class="container pt-5">
            <h2 class="text-white">Contact</h2>
            <h5 class="text-secondary">Association of New Generation LYCs (ANGL)</h5>
            <span class="d-block">Phone: 09767682526, 09767682526</span>
            <span>Email: mailtohtetphyonaing@gmail.com</span>
        </div>
    </footer>
    <!-- end footer -->

    <!-- bootstrap -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- slick js -->
    <script type="text/javascript" src={{ asset("assets/slick/slick.min.js") }}></script>

    <!-- icons -->
    <script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>

    <!-- jquery -->
    {{-- <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script> --}}

    <!-- fancybox js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.js"></script>

    {{-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script> --}}

    <script>
        function goToAbout() {
            window.location.assign('/about');
        }

        function goToPjsAndAct() {
            window.location.assign('/projects-and-activities');
        }

        function myFunction() {
            var x = document.getElementById("myTopnav");
            var y = document.getElementById("bar-icon");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        }

        $(function() {
            $(document).scroll(function() {
                var $nav = $(".topnav");
                $nav.toggleClass("scrolled", $(this).scrollTop() > $nav.height());
            });
        });

        // slick js
        $(document).ready(function() {
            $('.lazy-carousel').slick({
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 3,
                prevArrow: "<img class='a-left control-c prev slick-prev' src='assets/img/icons/left-arrow.svg'>",
                nextArrow: "<img class='slick-next' src='assets/img/icons/right-arrow.svg'>"
            });

            $('.left').on('click', function() {
                $('.slick-slider').slick('slickPrev');
            })

            $('.right').click(function() {
                $('.slick-slider').slick('slickNext');
            })
        });

        $(window).scroll(function() {
        var $nav = $(".topnav");
        var stickySidebar = $("#my-scrollspy").offset().top;
        // var stickySidebar = $("#my-scrollspy").offset().top;

        if ($(window).scrollTop() < stickySidebar) {
          $("#my-scrollspy").addClass("affix");
        } else {
          $("#my-scrollspy").removeClass("affix");
        }
      });
    </script>
</body>

</html>