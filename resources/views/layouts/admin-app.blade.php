<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Responsive Bootstrap 4 Dashboard Template">
    <meta name="author" content="ThemePixels">

    <!-- favicon -->
    <link rel="icon" href={{ asset("favicon.ico") }} />

    <title>DashForge Responsive Bootstrap 4 Dashboard Template</title>

    <!-- vendor css -->
    <link href={{ asset("assets/lib/%40fortawesome/fontawesome-free/css/all.min.css") }} rel="stylesheet">
    <link href={{ asset("assets/lib/ionicons/css/ionicons.min.css") }} rel="stylesheet">

    <!-- CSS -->
    <link rel="stylesheet" href={{ asset("assets/css/admin/dashforge.css") }}>
    <link rel="stylesheet" href={{ asset("assets/css/admin/style.css") }}>
    <link rel="stylesheet" href={{ asset("assets/css/admin/dashforge.dashboard.css") }}>

    <!-- quill text editor -->
    {{-- <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet"> --}}

    <!-- datepicker -> https://gijgo.com/datepicker/example/bootstrap-4 -->
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

  </head>
  <body>

    <aside class="aside aside-fixed">
        <div class="aside-header">
            <a href="#" class="aside-logo">ANGL<span>dash</span></a>
            <a href="#" class="aside-menu-link">
                <i data-feather="menu"></i>
                <i data-feather="x"></i>
            </a>
        </div>
        <div class="aside-body">
            <ul class="nav nav-aside">
                <li class="nav-label">
                    <span>Posts</span>
                </li>
                <li class="nav-item {{ (Request::is('apple-orange/admin/posts/create')) ? 'active' : '' }}">
                    <a href="{{ url('/apple-orange/admin/posts/create') }}" class="nav-link">
                        <i data-feather="edit"></i> <span>Upload Post</span>
                    </a>
                </li>
                <li class="nav-item {{ (Request::is('apple-orange/admin/posts')) ? 'active' : '' }}">
                    <a href="{{ url('/apple-orange/admin/posts') }}" class="nav-link">
                        <i data-feather="settings"></i> <span>Manage Posts</span>
                    </a>
                </li>
            </ul>
        </div>
    </aside> <!-- end aside end -->

    <div class="content pd-0">
        <div class="content-header fixed">
            <div class="content-search">
                <span class="content-header-active-title">Post</span>
            </div>
            <nav class="nav">
                <div class="dropdown dropdown-profile">
                    <a href="#" class="dropdown-link" data-toggle="dropdown" data-display="static">
                        <div class="avatar avatar-sm"><img src="assets/img/img1.png"
                                class="rounded-circle" alt="">
                        </div>
                    </a><!-- dropdown-link -->
                    <div class="dropdown-menu dropdown-menu-right tx-13">
                        <div class="avatar avatar-lg mg-b-15"><img src="assets/img/img1.png"
                                class="rounded-circle" alt=""></div>
                        <h6 class="tx-semibold mg-b-5">Katherine Pechon</h6>
                        <p class="mg-b-25 tx-12 tx-color-03">Administrator</p>
                        <div class="dropdown-divider"></div>
                        <a href="users/logout" class="dropdown-item">
                            <i data-feather="log-out"></i>Sign Out</a>
                    </div><!-- dropdown-menu -->
                </div><!-- dropdown -->
            </nav>
        </div><!-- content-header -->

        <div class="container pd-x-100">

            @if(Request::is('apple-orange/admin/posts/create'))   
                @yield('post_create')
            @elseif(Request::is('apple-orange/admin/posts'))
                @yield('posts')
            @endif

        </div>
    </div>
    
    


    <script src={{ asset("assets/lib/jquery/jquery.min.js") }}></script>
    <script src={{ asset("assets/lib/bootstrap/js/bootstrap.bundle.min.js") }}></script>
    <script src={{ asset("assets/lib/feather-icons/feather.min.js") }}></script>
    <script src={{ asset("assets/lib/perfect-scrollbar/perfect-scrollbar.min.js") }}></script>

    <script src={{ asset("assets/js/admin/dashforge.js") }}></script>
    <script src={{ asset("assets/js/admin/dashforge.aside.js") }}></script>
    <script src={{ asset("assets/js/admin/dashforge.sampledata.js") }}></script>

    <script src={{ asset("assets/lib/js-cookie/js.cookie.js") }}></script>
    <script src={{ asset("assets/js/admin/dashforge.settings.js") }}></script>

    <!-- quill text editor -->
    {{-- <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script> --}}

    <!-- datepicker -> https://gijgo.com/datepicker/example/bootstrap-4 -->
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>

    <script>
        $('#datepicker').datepicker({
            format: 'yyyy-mm-dd',
            uiLibrary: 'bootstrap4'
        });
    </script>

    <script>

    // get content from quill content
    var form = document.querySelector('form');

    form.onsubmit = function() {
    
    var body = document.querySelector('input[name=body]');
    body.value = JSON.stringify(quill.getContents());

    return true;
    };

    </script>

    <script>
        // text editor
        var quill = new Quill('#editor-container', {
            modules: {
                toolbar: '#toolbar-container'
            },
            placeholder: 'Here you write your content . . . A for Apple, B for Ball',
            theme: 'snow'
        });

        // scrollbar
        const scroll1 = new PerfectScrollbar('#scroll1', {
            suppressScrollX: true
        });
    </script>

    <script type="application/javascript">
    // bootstrap file name showing
        $('input[type="file"]').change(function(e){
            var fileName = e.target.files[0].name;
            $('.custom-file-label').html(fileName);
        });
    </script>

  </body>

</html>