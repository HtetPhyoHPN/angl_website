@extends('layouts.app')

@section('content')

<!-- # background image -->
<div class="bgimg-half">
    <div class="home-logozation">
        <h2 class="caption-logo mb-3">
            {{ $activity->year }} Projects and Activities
        </h2>
        <span class="whole-back-link">
             <i>
                <a href="/" class="back-link">Home</a> / {{ $activity->year }} Projects & Activities
            </i>
        </span>
    </div>
</div>
<!-- end background image -->

<!-- # activity -->
<section class="mt-5 about-section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-9">
                    <div class="sub-section">
                    <h2><a class="anchor" id="what-is-angl"># {{ $activity->title }}</a></h2>
                    <h5 class="text-secondary" style="display: inline;">{{ $activity->date }}</h5> . <h5 class="text-secondary" style="display: inline;">{{ $activity->user->university->name }}</h5>
                    
                    <br><br>
                    
                    <p class="text-body">{!! $activity->body !!}</p>

                    <div class="project-content mt-5">
                        @foreach($activity->thumbnails as $thumbnail)
                            <a data="{{ $activity->title }}" class="proCont" href="../../storage/images/{{ $thumbnail }}" data-fancybox="gallery" data-caption="{{ $activity->title }}">
                                <img src="../../storage/thumbnails/{{ $thumbnail }}" alt="{{ $activity->title }}">
                            </a>
                        @endforeach
                      </div>
                </div>

            </div>
            <div class="col-md-3 d-none d-md-block">
                <div id="my-scrollspy">
                    <div class="latest-news">
                        <h4>Recent Activities</h4>

                        @foreach($activity['latest-activities'] as $activity)
                        <div class="latest-news-item">
                            <a href="/projects-and-activities/{{ $activity->year }}/{{ $activity->id }}">
                                <p>{{ $activity->title }}</p>
                                <p><ion-icon class="align-middle mr-1" name="calendar"></ion-icon>{{ $activity->date }}</p>
                            </a>
                        </div>
                        @endforeach

                        <a href="/projects-and-activities">
                            <button class="w-100 mt-2 btn-all-activities">
                                All Activities
                            </button>
                        </a>

                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- end activity -->

@endsection