@extends('layouts.app')

@section('content')

<!-- # background image -->
<div class="bgimg-half">
    <div class="home-logozation">
        <h2 class="caption-logo mb-3">
        {{ $activities->year }} Projects and Activities
        </h2>
        <span class="whole-back-link">
            <i>
                <a href="index.html" class="back-link">Home</a> / Projects & Activities
            </i>
        </span>
    </div>
</div>
<!-- end background image -->

<!-- # activities -->
<section class="mt-5 news">
    <div class="container pt-4">

            <div class="d-flex align-content-stretch flex-wrap">
                
                @foreach($activities as $activity)
                
                <div class="col-12 col-md-4">
                    <div class="img-wrap">
                        <a href="/projects-and-activities/{{ $activity->year }}/{{ $activity->id }}">
                            <img src="../../storage/thumbnails/{{ $activity->thumbnails[0] }}" class="card-img">
                            <p class="p-3 img-description">{{ $activity->user->university->name }}</p>
                        </a>
                    </div>
                    <a href="/projects-and-activities/{{ $activity->year }}/{{ $activity->id }}">
                        <h5 class="news-title text-center mt-3">{{ $activity->title }}</h5>
                    </a>
                    <p class="text-secondary text-center mt-1">{{ $activity->date }}</p>
                </div>
                @endforeach

            </div>

    </div>
</section>
<!-- end activities -->

{{-- pagination --}}
<div class="row justify-content-center mt-5">
    {{ $activities->links() }}
</div>
{{-- pagination --}}


@endsection