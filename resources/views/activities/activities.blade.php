@extends('layouts.app')

@section('content')

<!-- # background image -->
<div class="bgimg-half">
    <div class="home-logozation">
        <h2 class="caption-logo mb-3">
            Projects and Activities
        </h2>
        <span class="whole-back-link">
            <i>
                <a href="index.html" class="back-link">Home</a> / Projects & Activities
            </i>
        </span>
    </div>
</div>
<!-- end background image -->

<!-- # 2019 activities -->
<section class="mt-5 news">
    <div class="container pt-4">

        <div class="row no-gutters pb-4 pb-md-5 px-3">
            <div class="col-">
                <h2 class="d-inline align-middle title">2019 Activities</h2>
            </div>
            <div class="col d-none d-md-block">
                <div class="line"></div>
            </div>
            <div class="col- d-none d-md-block">
                <a href="/projects-and-activities/2019">
                    <button class="btn my-btn">See All Activities</button>
                </a>
            </div>
        </div>

            <div class="d-flex align-content-stretch flex-wrap">
                
                @foreach($activities['2019'] as $activity)
                <div class="col-12 col-md-4">
                    <div class="img-wrap">
                        <a href="/projects-and-activities/{{ $activity->year }}/{{ $activity->id }}">
                            <img src="../storage/thumbnails/{{ $activity->thumbnails[0] }}" class="card-img">
                            <p class="p-3 img-description">{{ $activity->user->university->name }}</p>
                        </a>
                    </div>
                    <a href="/projects-and-activities/{{ $activity->year }}/{{ $activity->id }}">
                        <h5 class="news-title text-center mt-3">{{ $activity->title }}</h5>
                    </a>
                    <p class="text-secondary text-center mt-1">{{ $activity->date }}</p>
                </div>
                @endforeach

            </div>

    </div>
</section>
<!-- end 2019 activities -->

<!-- # 2018 activities -->
<section class="mt-5 news old-activities">
    <div class="container pt-4">

        <div class="row no-gutters pb-4 pb-md-5 px-3">
            <div class="col-">
                <h2 class="d-inline align-middle title">2018 Activities</h2>
            </div>
            <div class="col d-none d-md-block">
                <div class="line"></div>
            </div>
            <div class="col- d-none d-md-block">
                <a href="/projects-and-activities/2018">
                    <button class="btn my-btn">See All Activities</button>
                </a>
            </div>
        </div>

            <div class="d-flex align-content-stretch flex-wrap">
                
                @foreach($activities['2018'] as $activity)
                    <div class="col-12 col-md-4">
                        <div class="img-wrap">
                            <a href="/projects-and-activities/{{ $activity->year }}/{{ $activity->id }}">
                                <img src="../storage/thumbnails/{{ $activity->thumbnails[0] }}" class="card-img">
                                <p class="p-3 img-description">{{ $activity->user->university->name }}</p>
                            </a>
                        </div>
                        <a href="/projects-and-activities/{{ $activity->year }}/{{ $activity->id }}">
                            <h5 class="news-title text-center mt-3">{{ $activity->title }}</h5>
                        </a>
                        <p class="text-secondary text-center mt-1">{{ $activity->date }}</p>
                    </div>
                @endforeach

            </div>

    </div>
</section>
<!-- end 2018 activities -->

<!-- # 2017 activities -->
<section class="mt-5 news old-activities">
    <div class="container pt-4">

        <div class="row no-gutters pb-4 pb-md-5 px-3">
            <div class="col-">
                <h2 class="d-inline align-middle title">2017 Activities</h2>
            </div>
            <div class="col d-none d-md-block">
                <div class="line"></div>
            </div>
            <div class="col- d-none d-md-block">
                <a href="/projects-and-activities/2017">
                    <button class="btn my-btn">See All Activities</button>
                </a>
            </div>
        </div>

            <div class="d-flex align-content-stretch flex-wrap">
                
                @foreach($activities['2017'] as $activity)
                    <div class="col-12 col-md-4">
                        <div class="img-wrap">
                            <a href="/projects-and-activities/{{ $activity->year }}/{{ $activity->id }}">
                                <img src="../storage/thumbnails/{{ $activity->thumbnails[0] }}" class="card-img">
                                <p class="p-3 img-description">{{ $activity->user->university->name }}</p>
                            </a>
                        </div>
                        <a href="/projects-and-activities/{{ $activity->year }}/{{ $activity->id }}">
                            <h5 class="news-title text-center mt-3">{{ $activity->title }}</h5>
                        </a>
                        <p class="text-secondary text-center mt-1">{{ $activity->date }}</p>
                    </div>
                @endforeach

            </div>

    </div>
</section>
<!-- end 2017 activities -->

<!-- # 2016 activities -->
<section class="mt-5 news old-activities">
    <div class="container pt-4">

        <div class="row no-gutters pb-4 pb-md-5 px-3">
            <div class="col-">
                <h2 class="d-inline align-middle title">2016 Activities</h2>
            </div>
            <div class="col d-none d-md-block">
                <div class="line"></div>
            </div>
            <div class="col- d-none d-md-block">
                <a href="/projects-and-activities/2016">
                    <button class="btn my-btn">See All Activities</button>
                </a>
            </div>
        </div>

            <div class="d-flex align-content-stretch flex-wrap">
                
                @foreach($activities['2016'] as $activity)
                    <div class="col-12 col-md-4">
                        <div class="img-wrap">
                            <a href="/projects-and-activities/{{ $activity->year }}/{{ $activity->id }}">
                                <img src="../storage/thumbnails/{{ $activity->thumbnails[0] }}" class="card-img">
                                <p class="p-3 img-description">{{ $activity->user->university->name }}</p>
                            </a>
                        </div>
                        <a href="/projects-and-activities/{{ $activity->year }}/{{ $activity->id }}">
                            <h5 class="news-title text-center mt-3">{{ $activity->title }}</h5>
                        </a>
                        <p class="text-secondary text-center mt-1">{{ $activity->date }}</p>
                    </div>
                @endforeach

            </div>

    </div>
</section>
<!-- end 2016 activities -->


@endsection