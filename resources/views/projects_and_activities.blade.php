@extends('layouts.app')

@section('content')

<!-- # background image -->
<div class="bgimg-half">
    <div class="home-logozation">
        <h2 class="caption-logo mb-3">
            Projects and Activities
        </h2>
        <span class="whole-back-link">
                <i>
                    <a href="index.html" class="back-link">Home</a> / Projects & Activities
                </i>
            </span>
    </div>
</div>
<!-- end background image -->

<!-- # 2019 activities -->
<section class="mt-5 news">
    <div class="container pt-4">
        <div class="row no-gutters">
            <div class="col-">
                <h2 class="d-inline align-middle title">2019 Activities</h2>
            </div>
            <div class="col d-none d-md-block">
                <div class="line"></div>
            </div>
            <div class="col- d-none d-md-block">
                <a href="/projects_and_activities/2019/index.html"><button class="btn my-btn">See All
                            Activities</button></a>
            </div>
        </div>

        <div class="row pt-4 pt-md-5">
            <div class="col-12 col-md-4">
                <!-- 1 -->
                <div class="">
                    <a href="#">
                        <img src="assets/img/news-1.jpg" class="card-img">
                    </a>
                    <a href="#">
                        <h5 class="news-title text-center mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit
                        </h5>
                    </a>
                    <p class="text-secondary text-center mt-1">29-3-2019</p>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <!-- 2 -->
                <div class="">
                    <a href="#">
                        <img src="assets/img/news-2.jpg" class="card-img">
                    </a>
                    <a href="#">
                        <h5 class="news-title text-center mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit incididunt ut.
                        </h5>
                    </a>
                    <p class="text-secondary text-center mt-1">29-3-2019</p>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <!-- 3 -->
                <div class="">
                    <a href="#">
                        <img src="assets/img/news-3.jpg" class="card-img">
                    </a>
                    <a href="#">
                        <h5 class="news-title text-center mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit incididunt ut labore ut
                        </h5>
                    </a>
                    <p class="text-secondary text-center mt-1">29-3-2019</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-md-4">
                <!-- 1 -->
                <div class="">
                    <a href="#">
                        <img src="assets/img/news-1.jpg" class="card-img">
                    </a>
                    <a href="#">
                        <h5 class="news-title text-center mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit
                        </h5>
                    </a>
                    <p class="text-secondary text-center mt-1">29-3-2019</p>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <!-- 2 -->
                <div class="">
                    <a href="#">
                        <img src="assets/img/news-2.jpg" class="card-img">
                    </a>
                    <a href="#">
                        <h5 class="news-title text-center mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit incididunt ut.
                        </h5>
                    </a>
                    <p class="text-secondary text-center mt-1">29-3-2019</p>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <!-- 3 -->
                <div class="">
                    <a href="#">
                        <img src="assets/img/news-3.jpg" class="card-img">
                    </a>
                    <a href="#">
                        <h5 class="news-title text-center mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit incididunt ut labore ut
                        </h5>
                    </a>
                    <p class="text-secondary text-center mt-1">29-3-2019</p>
                </div>
            </div>

            <div class="col d-block d-md-none">
                <div class="line"></div>
            </div>

            <div class="col-12 text-center d-block d-md-none">
                <a href="projects_and_activities/2019/index.html"><button class="btn my-btn text-center">See All
                            Activities</button></a>
            </div>
        </div>
    </div>
</section>
<!-- end 2019 activities -->

 <!-- # 2018 activities -->
 <section class="mt-5 news old-activities">
    <div class="container pt-4">
        <div class="row no-gutters">
            <div class="col-">
                <h2 class="d-inline align-middle title">2018 Activities</h2>
            </div>
            <div class="col d-none d-md-block">
                <div class="line"></div>
            </div>
            <div class="col- d-none d-md-block">
                <a href="#"><button class="btn my-btn">See All Activities</button></a>
            </div>
        </div>

        <div class="row pt-4 pt-md-5">
            <div class="col-12 col-md-4">
                <!-- 1 -->
                <div class="">
                    <a href="#">
                        <img src="assets/img/news-1.jpg" class="card-img">
                    </a>
                    <a href="#">
                        <h5 class="news-title text-center mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit
                        </h5>
                    </a>
                    <p class="text-secondary text-center mt-1">29-3-2019</p>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <!-- 2 -->
                <div class="">
                    <a href="#">
                        <img src="assets/img/news-2.jpg" class="card-img">
                    </a>
                    <a href="#">
                        <h5 class="news-title text-center mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit incididunt ut.
                        </h5>
                    </a>
                    <p class="text-secondary text-center mt-1">29-3-2019</p>
                </div>
            </div>


            <div class="col-12 col-md-4">
                <!-- 3 -->
                <div class="">
                    <a href="#">
                        <img src="assets/img/news-3.jpg" class="card-img">
                    </a>
                    <a href="#">
                        <h5 class="news-title text-center mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit incididunt ut labore ut
                        </h5>
                    </a>
                    <p class="text-secondary text-center mt-1">29-3-2019</p>
                </div>
            </div>

            <div class="col d-block d-md-none">
                <div class="line"></div>
            </div>

            <div class="col-12 text-center d-block d-md-none">
                <a href="#"><button class="btn my-btn text-center">See All Activities</button></a>
            </div>
        </div>

    </div>
</section>
<!-- end 2018 activities -->

<!-- # 2017 activities -->
<section class="mt-5 news old-activities">
    <div class="container pt-4">
        <div class="row no-gutters">
            <div class="col-">
                <h2 class="d-inline align-middle title">2017 Activities</h2>
            </div>
            <div class="col d-none d-md-block">
                <div class="line"></div>
            </div>
            <div class="col- d-none d-md-block">
                <a href="#"><button class="btn my-btn">See All Activities</button></a>
            </div>
        </div>

        <div class="row pt-4 pt-md-5">
            <div class="col-12 col-md-4">
                <!-- 1 -->
                <div class="">
                    <a href="#">
                        <img src="assets/img/news-1.jpg" class="card-img">
                    </a>
                    <a href="#">
                        <h5 class="news-title text-center mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit
                        </h5>
                    </a>
                    <p class="text-secondary text-center mt-1">29-3-2019</p>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <!-- 2 -->
                <div class="">
                    <a href="#">
                        <img src="assets/img/news-2.jpg" class="card-img">
                    </a>
                    <a href="#">
                        <h5 class="news-title text-center mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit incididunt ut.
                        </h5>
                    </a>
                    <p class="text-secondary text-center mt-1">29-3-2019</p>
                </div>
            </div>


            <div class="col-12 col-md-4">
                <!-- 3 -->
                <div class="">
                    <a href="#">
                        <img src="assets/img/news-3.jpg" class="card-img">
                    </a>
                    <a href="#">
                        <h5 class="news-title text-center mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit incididunt ut labore ut
                        </h5>
                    </a>
                    <p class="text-secondary text-center mt-1">29-3-2019</p>
                </div>
            </div>

            <div class="col d-block d-md-none">
                <div class="line"></div>
            </div>

            <div class="col-12 text-center d-block d-md-none">
                <a href="#"><button class="btn my-btn text-center">See All Activities</button></a>
            </div>
        </div>

    </div>
</section>
<!-- end 2017 activities -->

<!-- # 2016 activities -->
<section class="mt-5 news old-activities">
    <div class="container pt-4">
        <div class="row no-gutters">
            <div class="col-">
                <h2 class="d-inline align-middle title">2016 Activities</h2>
            </div>
            <div class="col d-none d-md-block">
                <div class="line"></div>
            </div>
            <div class="col- d-none d-md-block">
                <a href="#"><button class="btn my-btn">See All Activities</button></a>
            </div>
        </div>

        <div class="row pt-4 pt-md-5">
            <div class="col-12 col-md-4">
                <!-- 1 -->
                <div class="">
                    <a href="#">
                        <img src="assets/img/news-1.jpg" class="card-img">
                    </a>
                    <a href="#">
                        <h5 class="news-title text-center mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit
                        </h5>
                    </a>
                    <p class="text-secondary text-center mt-1">29-3-2019</p>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <!-- 2 -->
                <div class="">
                    <a href="#">
                        <img src="assets/img/news-2.jpg" class="card-img">
                    </a>
                    <a href="#">
                        <h5 class="news-title text-center mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit incididunt ut.
                        </h5>
                    </a>
                    <p class="text-secondary text-center mt-1">29-3-2019</p>
                </div>
            </div>


            <div class="col-12 col-md-4">
                <!-- 3 -->
                <div class="">
                    <a href="#">
                        <img src="assets/img/news-3.jpg" class="card-img">
                    </a>
                    <a href="#">
                        <h5 class="news-title text-center mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit incididunt ut labore ut
                        </h5>
                    </a>
                    <p class="text-secondary text-center mt-1">29-3-2019</p>
                </div>
            </div>

            <div class="col d-block d-md-none">
                <div class="line"></div>
            </div>

            <div class="col-12 text-center d-block d-md-none">
                <a href="#"><button class="btn my-btn text-center">See All Activities</button></a>
            </div>
        </div>

    </div>
</section>
<!-- end 2016 activities -->



@endsection