@extends('layouts.app')

@section('content')

<!-- # background image -->
<div class="bgimg-1">
    <div class="home-logozation">
        <img src="assets/img/icons/logo.png" class="home-logo" />
        <h2 class="caption-logo mb-3">
            <!-- Association of New Generation LuYeeChuns (ANGL) -->
            မျိုးဆက်သစ်လူရည်ချွန်များအဖွဲ့ (ဗဟို)
        </h2>
        <span class="span-logo"><i>"Once LYC, Forever LYC"</i></span>
    </div>
</div>
<!-- end background image -->

<!-- # what angl is -->
<section class="mt-3 what-angl-is">
    <div class="container pt-3">
        <div class="row">
            <div class="col-12 col-md-6">
                <h2 class="mb-0">What ANGL is</h2>
                <!-- <div class="h2-hr d-block d-sm-none"></div> -->
                <p class="p-myanmar-font">
                    In reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. In reprehenderit in voluptate velit esse cillum dolore eu fugiat.
                </p>
                <p>
                    In reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
                <p>
                    Reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum ullamco laboris nisi ut aliquip ex ea commodo consequat.
                </p>
                <hr class="my-hr">
                <button class="btn my-btn btn-on-hr"><a class="anchor text-white" style="text-decoration: none;"
                            href="/about#what-is-angl">Learn
                            More</a></button>
            </div>

            <div class="col-12 col-md-6">
                <div class="row">
                    <div class="col-12">
                        <h2>Objectives</h2>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo . . .
                        </p>
                        <hr class="my-hr">
                        <button class="btn my-btn btn-on-hr"><a class="anchor text-white"
                                    style="text-decoration: none;" href="/about#mission">Learn
                                    More</a></button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <h2>History</h2>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo. exercitation ullamco laboris
                            nisi ut aliquip ex ea commodo . . .
                        </p>
                        <hr class="my-hr">
                        <button class="btn my-btn btn-on-hr"><a class="anchor text-white"
                                    style="text-decoration: none;" href="/about#history">Learn
                                    More</a></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end what angl is -->

<!-- # reagional organizations -->
<section class="mt-5 reagional-organizations">
    <div class="container py-5">
        <h2>Reagional Organizations</h2>
        <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore ut labore et dolore Lorem ipsum
            dolor sit amet.
        </p>

        <!-- # yangon & mandalay row -->
        <div class="row">
            <!-- yangon -->
            <div class="col-12 col-md-6 region-div">
                <h5>Yangon</h5>
                <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore . . . <span class="badge badge-pill badge-secondary"><a href="regions/yangon.html"
                                class="text-white decoration-none" style="text-decoration: none;">Learn
                                More</a></span>
                </p>

                <div class="lazy-carousel">
                    <img src="assets/img/icons/regionals_logos/ygn/mmu.jpg" class="mr-1 ml-1 slick-slider-img" />
                    <img src="assets/img/icons/regionals_logos/ygn/nmdc.png" class="mr-1 ml-1 slick-slider-img" />
                    <img src="assets/img/icons/regionals_logos/ygn/ttu.png" class="mr-1 ml-1 slick-slider-img" />
                    <img src="assets/img/icons/regionals_logos/ygn/uit.png" class="mr-1 ml-1 slick-slider-img" />
                    <img src="assets/img/icons/regionals_logos/ygn/um1.png" class="mr-1 ml-1 slick-slider-img" />
                    <img src="assets/img/icons/regionals_logos/ygn/um2.jpg" class="mr-1 ml-1 slick-slider-img" />
                    <img src="assets/img/icons/regionals_logos/ygn/umdy.jpeg" class="mr-1 ml-1 slick-slider-img" />
                    <img src="assets/img/icons/regionals_logos/ygn/ytu.jpg" class="mr-1 ml-1 slick-slider-img" />
                    <img src="assets/img/icons/regionals_logos/ygn/yu.jpeg" class="mr-1 ml-1 slick-slider-img" />
                    <img src="assets/img/icons/regionals_logos/ygn/yufl.jpg" class="mr-1 ml-1 slick-slider-img" />
                </div>

            </div>
            <!-- mandalay -->
            <div class="col-12 col-md-6 region-div region-sec-column">
                <h5>Mandalay</h5>
                <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore . . . <span class="badge badge-pill badge-secondary"><a href="regions/mandalay.html"
                                class="text-white decoration-none" style="text-decoration: none;">Learn
                                More</a></span></p>

                <div class="lazy-carousel">
                    <img src="assets/img/icons/regionals_logos/mandalay/mdl.jpg" class="mr-1 ml-1 slick-slider-img" />
                    <img src="assets/img/icons/regionals_logos/mandalay/miit.jpg" class="mr-1 ml-1 slick-slider-img" />
                    <img src="assets/img/icons/regionals_logos/mandalay/mmu.jpg" class="mr-1 ml-1 slick-slider-img" />
                    <img src="assets/img/icons/regionals_logos/mandalay/mtu.png" class="mr-1 ml-1 slick-slider-img" />
                    <img src="assets/img/icons/regionals_logos/mandalay/tum.jpeg" class="mr-1 ml-1 slick-slider-img" />
                    <img src="assets/img/icons/regionals_logos/mandalay/udmm.jpg" class="mr-1 ml-1 slick-slider-img" />
                    <img src="assets/img/icons/regionals_logos/mandalay/umm.JPG" class="mr-1 ml-1 slick-slider-img" />
                    <img src="assets/img/icons/regionals_logos/mandalay/utycc.jpg" class="mr-1 ml-1 slick-slider-img" />
                </div>
            </div>
        </div>
        <!-- end yangon & mandalay row -->

        <!-- # taungyi & mgway row -->
        <div class="row mt-4">
            <!-- taungyi -->
            <div class="col-12 col-md-6 region-div">
                <h5>Taungyi</h5>
                <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore . . . <span class="badge badge-pill badge-secondary"><a href="regions/taungyi.html"
                                class="text-white decoration-none" style="text-decoration: none;">Learn
                                More</a></span></p>

                <div class="lazy-carousel">
                    <img src="assets/img/icons/regionals_logos/taungyi/umtgi.png" class="mr-1 ml-1 slick-slider-img" />
                    <img src="assets/img/icons/regionals_logos/regional-orgs.jpg" class="mr-1 ml-1 slick-slider-img" />
                </div>

            </div>
            <!-- mgway -->
            <div class="col-12 col-md-6 region-div region-sec-column">
                <h5>Mgway</h5>
                <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore . . . <span class="badge badge-pill badge-secondary"><a href="regions/magway.html"
                                class="text-white decoration-none" style="text-decoration: none;">Learn
                                More</a></span></p>

                <div class="lazy-carousel">
                    <img src="assets/img/icons/regionals_logos/mgway/ummg.jpeg" class="mr-1 ml-1 slick-slider-img float-left" />
                </div>
            </div>
        </div>
        <!-- end Taungyi & Mgway row -->
    </div>
</section>
<!-- end reagional organizations -->

<!-- # activities -->
<section class="mt-5 news">
    <div class="container pt-4">
        <div class="row no-gutters">
            <div class="col-">
                <h2 class="d-inline align-middle title">Activities</h2>
            </div>
            <div class="col">
                <div class="line"></div>
            </div>
            <div class="col-">
                <a href="#"><button class="btn my-btn">See All Activities</button></a>
            </div>
        </div>

        <div class="row pt-5">
            <div class="col-12 col-md-4">
                <!-- 1 -->
                <div class="">
                    <a href="#">
                        <img src="assets/img/news-1.jpg" class="card-img">
                    </a>
                    <a href="#">
                        <h5 class="news-title text-center mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit
                        </h5>
                    </a>
                    <p class="text-secondary text-center mt-3">29-3-2019</p>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <!-- 2 -->
                <div class="">
                    <a href="#">
                        <img src="assets/img/news-2.jpg" class="card-img">
                    </a>
                    <a href="#">
                        <h5 class="news-title text-center mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit incididunt ut.
                        </h5>
                    </a>
                    <p class="text-secondary text-center mt-3">29-3-2019</p>
                </div>
            </div>


            <div class="col-12 col-md-4">
                <!-- 3 -->
                <div class="">
                    <a href="#">
                        <img src="assets/img/news-3.jpg" class="card-img">
                    </a>
                    <a href="#">
                        <h5 class="news-title text-center mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit incididunt ut labore ut
                        </h5>
                    </a>
                    <p class="text-secondary text-center mt-3">29-3-2019</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end activities -->

<!-- # get the book -->
<section class="mt-5 get-the-book">
    <div class="container py-5">
        <div class="row no-gutters">
            <div class="col-12 col-md-6">
                <img src="assets/img/book-covers.png" alt="" class="book-covers float-right mr-5">
            </div>
            <div class="col-12 col-md-6">
                <div class="get-the-book-content">
                    <h3 class="get-the-book-heading">Get the book of Activities</h3>
                    <p class="text-dark">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem, atque consectetur nemo eaque neque deserunt illum debitis voluptates aliquid! Quas exercitationem voluptate vel consequuntur placeat repellendus rem, magni illum. Quidem.
                    </p>
                    <hr class="my-hr">
                    <button class="btn my-btn btn-on-hr">Get the book</button>
                    <!-- default -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end get the book -->

<!-- # stay update -->
 <section class="mt-5 pb-5">
    <div class="row pt-3 no-gutters text-center">
        <div class="col-12">
            <div class="hexagon">
                <img src="assets/img/icons/envelope.png" class="mail-icon">
            </div>
            <h4 class="pt-3 h4-stay-update">Stay Updated!</h4>
            <p class="text-center text-secondary subscribe-text">Subscribe for the newsletter converniely marph suphier sources.
            </p>

            <form action="#" class="stay-update">
                <div class="row no-gutters justify-content-center">
                    <input type="email" class="email" placeholder="Your email">
                    <button class="btn my-btn" type="submit">Subscribe</button>
                </div>
            </form>
        </div>
    </div>
</section>
<!-- end stay update -->

@endsection