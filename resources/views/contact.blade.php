@extends('layouts.app')

@section('content')

<!-- # background image -->
<div class="bgimg-half">
    <div class="home-logozation">
        <h2 class="caption-logo mb-3">
            Contact
        </h2>
        <span class="whole-back-link">
            <i>
                <a href="index.html" class="back-link">Home</a> / Contact
            </i>
        </span>
    </div>
</div>
<!-- end background image -->

@endsection