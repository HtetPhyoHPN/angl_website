@extends('layouts.admin-app');

@section('post_create')

<form action="/apple-orange/admin/posts" method="POST" id="my-form" enctype="multipart/form-data">

    @csrf

    <!-- title -->
    <div class="mb-4 upload-post-title">
        <label for="title">Title <span class="text-danger">*</span></label>
        <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" placeholder="Title" id="title" autofocus value={{ old('title') }}>
        @error('title')
        <div class="mt-1">
            <span class="text-danger">{{ $message }}</span>
        </div>
        @enderror
    </div>

    <!-- body -->
    <div class="mb-4 form-group">
        <label for="body">Body <span class="text-danger">*</span></label>
        <textarea class="form-control @error('body') is-invalid @enderror" placeholder="Here you write your awesome content . . ." id="body" rows="6" name="body">{{ old('body') }}</textarea>
        @error('body')
        <div class="mt-1">
            <span class="text-danger">{{ $message }}</span>
        </div>
        @enderror
    </div>

    <div class="row">
        <div class="col-12 col-md-6">
            <!-- date -->
            <h6 class="mb-2">Date <span class="text-danger">*</span></h6>
            <div class="mb-4">
                <input name="date" id="datepicker" class="form-control @error('date') is-invalid @enderror" width="100%" placeholder="Click on the icon to select." readonly value={{ old('date') }}
                >
                @error('date')
                <div class="mt-1">
                    <span class="text-danger">{{ $message }}</span>
                </div>
                @enderror
            </div>
        </div>
        <div class="col-12 col-md-6">
            <!-- image -->
            <h6 class="mb-2">Images <span class="text-danger">*</span> <span class="text-muted text-small"><i>(must be type of jpg, jpeg, bmp or png)</i></span></h6>
            <div class="mb-4 custom-file">
                <input type="file" name="image[]" class="custom-file-input form-control @error('image') is-invalid @enderror" multiple="multiple" id="customFile" style="background-color: rgba(214, 242, 255, 0.363) !important;">
                <label class="custom-file-label" for="customFile">Choose file</label>
                @error('image')
                    <div class="mt-1">
                        <span class="text-danger">{{ $message }}</span>
                    </div>
                @enderror
            </div>
        </div>
    </div>

    <button type="submit" class="btn btn-primary mt-3" name="submit">Submit Information</button>

</form>

@include('../flash-messages/flash-message')

@endsection