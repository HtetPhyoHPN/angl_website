@extends('layouts.admin-app');

@section('posts')

<div style="margin-top: 100px !important;" class="mb-5">

    @include('../flash-messages/flash-message')

    <h4 class="d-inline-block">{{ $posts['this_year']['year'] }} Activities </h4><i>({{ $posts['this_year']['count']}})</i>

    <table class="w-100 table-hover table-sm">
        <tr class="my-bg-light">
            <th>No</th>
            <th>Title</th>
            <th>Date</th>
            <th>Action</th>
            <th>Action</th>
        </tr>
        @foreach($posts['this_year']['posts'] as $post)
        <tr>
            <td>{{ $post->id }}</td>
            <td title="{{ $post->body }}">{{ $post->title }}</td>
            {{-- <td title="{{ (strlen($post->body) > 13) ? substr($post->body,0,10).'...' : $post->body }}">{{ $post->title }}</td> --}}
            <td>{{ $post->date }}</td>
            <td><a href="posts/{{ $post->id }}/edit"><span class="badge badge-pill badge-my-warning">Edit</span></a></td>
            <form method="POST" action="posts/{{ $post->id }}"> 
                @csrf
                @method('DELETE')
                <td><button type="submit" style="background-color: transparent; border: 0;" onclick="return confirm('Are you sure to permanently delete the post?')"><span class="badge badge-pill badge-my-danger">Delete</span></button></td>
            </form>
        </tr>
        @endforeach
    </table>

    <h4 class="d-inline-block mt-4">{{ $posts['prev_year']['year'] }} Activities </h4><i>({{ $posts['prev_year']['count']}})</i>

    <table class="w-100 table-hover table-sm">
        <tr class="my-bg-light">
            <th>No</th>
            <th>Title</th>
            <th>Date</th>
            <th>Action</th>
            <th>Action</th>
        </tr>
        @foreach($posts['prev_year']['posts'] as $post)
        <tr>
            <td>{{ $post->id }}</td>
            <td title="{{ $post->body }}">{{ $post->title }}</td>
            {{-- <td title="{{ (strlen($post->body) > 13) ? substr($post->body,0,10).'...' : $post->body }}">{{ $post->title }}</td> --}}
            <td>{{ $post->date }}</td>
            <td><a href="posts/{{ $post->id }}/edit"><span class="badge badge-pill badge-my-warning">Edit</span></a></td>
            <form method="POST" action="posts/{{ $post->id }}"> 
                @csrf
                @method('DELETE')
                <td><button type="submit" style="background-color: transparent; border: 0;" onclick="return confirm('Are you sure to permanently delete the post?')"><span class="badge badge-pill badge-my-danger">Delete</span></button></td>
            </form>
        </tr>
        @endforeach
    </table>
</div>



@endsection