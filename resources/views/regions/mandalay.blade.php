@extends('layouts.app')

@section('content')

<!-- # background image -->
<div class="bgimg-half">
    <div class="home-logozation">
        <h2 class="caption-logo mb-3">
            Mandalay Region
        </h2>
        <span class="whole-back-link">
            <i>
                <a href="index.html" class="back-link">Home</a> / Region / Mandalay
            </i>
        </span>
    </div>
</div>
<!-- end background image -->

<!-- # main content -->
<section class="mt-5 about-section">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-9">
          <div class="sub-section">
            <h2><a># This is Mandalay Region</a></h2>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Perferendis quo aut adipisci sapiente minima molestias
              distinctio! Eos, atque voluptates libero ut impedit, laborum
              deleniti dolore aliquam pariatur unde, corporis explicabo?
            </p>
          </div>

          <div class="sub-section">
            <div class="row">
              <div class="col-md-3">
                <img
                  class="align-self-start res-200-200"
                  src="../assets/img/icons/regionals_logos/ygn/mmu.jpg"
                  alt="Generic placeholder image"
                />
              </div>
              <div class="col-md-9 pl-3 pl-md-5 pt-3 pt-md-0">
                <h5 class="mt-0"><b>University of Blah</b></h5>
                <div class="my-underline mb-3"></div>
                <p>
                  Cras sit amet nibh libero, in gravida nulla. Nulla vel metus
                  scelerisque ante sollicitudin. Cras purus odio, vestibulum
                  in vulputate at, tempus viverra turpis. Fusce condimentum
                  nunc ac nisi vulputate fringilla. Donec lacinia congue felis
                  in faucibus.
                </p>
                <span class="fb-link"
                  ><a href="https://www.facebook.com/CyberYatanarpon/"
                    >View on Facebook</a
                  ></span
                >
              </div>
            </div>

            <div class="row mt-4">
              <div class="col-md-3">
                <img
                  class="align-self-start res-200-200"
                  src="../assets/img/icons/regionals_logos/ygn/nmdc.png"
                  alt="Generic placeholder image"
                />
              </div>
              <div class="col-md-9 pl-3 pl-md-5 pt-3 pt-md-0">
                <h5 class="mt-0"><b>University of Blah Blah</b></h5>
                <div class="my-underline mb-3"></div>
                <p>
                  Cras sit amet nibh libero, in gravida nulla. Nulla vel metus
                  scelerisque ante sollicitudin. Cras purus odio.
                </p>
                <span class="fb-link"
                  ><a href="https://www.facebook.com/CyberYatanarpon/"
                    >View on Facebook</a
                  ></span
                >
              </div>
            </div>

            <div class="row mt-4">
              <div class="col-md-3">
                <img
                  class="align-self-start res-200-200"
                  src="../assets/img/icons/regionals_logos/ygn/ttu.png"
                  alt="Generic placeholder image"
                />
              </div>
              <div class="col-md-9 pl-3 pl-md-5 pt-3 pt-md-0">
                <h5 class="mt-0"><b>University of Blah - Oh</b></h5>
                <div class="my-underline mb-3"></div>
                <p>
                  Cras sit amet nibh libero, in gravida nulla. Nulla vel metus
                  scelerisque ante sollicitudin. Cras purus odio.
                </p>
                <span class="fb-link"
                  ><a href="https://www.facebook.com/CyberYatanarpon/"
                    >View on Facebook</a
                  ></span
                >
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
<!-- end main content -->

@endsection